package ro.ionutscheianu.tema2;

import ro.ionutscheianu.tema2.graph.Graph;
import ro.ionutscheianu.tema2.graph.MaxFlowResult;

public class Main {
    public static void main(String[] args) {
        Graph graph = new Graph();
        System.out.println(graph);
        MaxFlowResult result = graph.getMaxFlow();
        System.out.println(result);
        result.printResult();
    }
}
