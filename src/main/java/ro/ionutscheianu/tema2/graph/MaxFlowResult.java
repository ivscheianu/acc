package ro.ionutscheianu.tema2.graph;

import java.util.List;

public class MaxFlowResult {

    private int maxFlowValue;
    private List<Integer> path;
    private int crewNumber;
    private int flightsNumber;

    public MaxFlowResult(int maxFlowValue, List<Integer> path, int crewNumber, int flightsNumber) {
        this.maxFlowValue = maxFlowValue;
        this.path = path;
        this.crewNumber = crewNumber;
        this.flightsNumber = flightsNumber;
    }

    public int getMaxFlowValue() {
        return maxFlowValue;
    }

    public void setMaxFlowValue(int maxFlowValue) {
        this.maxFlowValue = maxFlowValue;
    }

    public List<Integer> getPath() {
        return path;
    }

    public void setPath(List<Integer> path) {
        this.path = path;
    }

    public int getCrewNumber() {
        return crewNumber;
    }

    public void setCrewNumber(int crewNumber) {
        this.crewNumber = crewNumber;
    }

    public int getFlightsNumber() {
        return flightsNumber;
    }

    public void setFlightsNumber(int flightsNumber) {
        this.flightsNumber = flightsNumber;
    }

    public void printResult(){
        for(int flightIndex=crewNumber+1;flightIndex<path.size()-1;++flightIndex){
            for(int crewIndex = 1; crewIndex<=crewNumber;++crewIndex){
                if(path.get(flightIndex) == crewIndex){
                    System.out.println("Crew " + crewIndex + " : " + (flightIndex - crewNumber) + " Flight");
                }
            }
        }
    }

    @Override
    public String toString() {
        return "MaxFlowResult{\n" +
                "maxFlowValue=" + maxFlowValue +
                ", \npath=" + path +
                ", \ncrewNumber=" + crewNumber +
                ", \nflightsNumber=" + flightsNumber +
                "\n}";
    }
}
