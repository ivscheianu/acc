package ro.ionutscheianu.tema2.graph;

import ro.ionutscheianu.tema2.input.InputToMatrixConverter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Graph {
    List<List<Integer>> adjacencyMatrix;
    private int crewNumber;
    private int flightsNumber;

    public Graph() {
        InputToMatrixConverter inputToMatrixConverter = new InputToMatrixConverter();
        adjacencyMatrix = inputToMatrixConverter.createAdjacencyListFromInput();
        crewNumber = inputToMatrixConverter.getCrewNumber();
        flightsNumber = inputToMatrixConverter.getFlightsNumber();
    }

    /**
     * @return the maximum flow from sourceNode to stockNode in the given graph
     */
    public MaxFlowResult getMaxFlow() {
        int sourceNode = 0;
        int stockNode = adjacencyMatrix.size() - 1;
        List<List<Integer>> residualGraph = new ArrayList<>(adjacencyMatrix);
        List<Integer> parent = new ArrayList<>(Collections.nCopies(adjacencyMatrix.size(), 0));
        int maxFlow = 0;
        // Augment the flow while there is path from sourceNode to sink
        while (breadthFirstSearch(residualGraph, sourceNode, stockNode, parent)) {
            int pathFlow = findMaxFlowForFoundPath(sourceNode, stockNode, residualGraph, parent);
            updateResidualCapacities(sourceNode, stockNode, residualGraph, parent, pathFlow);
            maxFlow += pathFlow;
        }
        return new MaxFlowResult(maxFlow, parent, crewNumber, flightsNumber);
    }

    /**
     * updates residual capacities of the edges and
     * reverse edges along the path
     *
     * @param sourceNode    the source node
     * @param stockNode     the stock node
     * @param residualGraph the residual graph
     * @param parent        the path array
     * @param pathFlow      max flow for found path
     */
    private void updateResidualCapacities(int sourceNode, int stockNode, List<List<Integer>> residualGraph, List<Integer> parent, int pathFlow) {
        int currentNode;
        int parentNode;
        for (currentNode = stockNode; currentNode != sourceNode; currentNode = parent.get(currentNode)) {
            parentNode = parent.get(currentNode);
            residualGraph.get(parentNode).set(currentNode, residualGraph.get(parentNode).get(currentNode) - pathFlow);
            residualGraph.get(currentNode).set(parentNode, residualGraph.get(currentNode).get(parentNode) + pathFlow);
        }
    }

    /**
     * Find minimum residual capacity of the edges
     * along the path filled by BFS. Or we can say
     * find the maximum flow through the path found.
     *
     * @param sourceNode    the source node
     * @param stockNode     the stock node
     * @param residualGraph the residual graph
     * @param parent        the path array
     * @return maxPathFlow
     */
    private int findMaxFlowForFoundPath(int sourceNode, int stockNode, List<List<Integer>> residualGraph, List<Integer> parent) {
        int currentNode;
        int parentNode;
        int pathFlow = Integer.MAX_VALUE;
        for (currentNode = stockNode; currentNode != sourceNode; currentNode = parent.get(currentNode)) {
            parentNode = parent.get(currentNode);
            pathFlow = Math.min(pathFlow, residualGraph.get(parentNode).get(currentNode));
        }
        return pathFlow;
    }

    /**
     * Returns true if there is a path from source sourceNode to sink
     * stockNode in residual graph. Also fills parent to store the path
     */
    private boolean breadthFirstSearch(List<List<Integer>> residualGraph, int sourceNode, int stockNode, List<Integer> parent) {
        List<Boolean> visited = new ArrayList<>(Collections.nCopies(adjacencyMatrix.size(), false));
        Queue<Integer> queue = breadthFirstSetup(sourceNode, parent, visited);
        breadthFirstLoop(residualGraph, parent, visited, queue);
        // If we reached sink in BFS starting from source, then return true, otherwise false
        return (visited.get(stockNode));
    }

    /**
     * Creates a queue, enqueue source node and marks it as visited
     * @param sourceNode the starting node
     * @param parent the path array
     * @param visited boolean array to keep a record of visited nodes
     * @return the queue with the source node inserted
     */
    private Queue<Integer> breadthFirstSetup(int sourceNode, List<Integer> parent, List<Boolean> visited) {
        Queue<Integer> queue = new LinkedList<>();
        queue.add(sourceNode);
        visited.set(sourceNode, true);
        parent.set(sourceNode, -1);
        return queue;
    }

    /**
     * standard BFS loop
     * @param residualGraph Residual graph where residualGraph.get(i).get(j) indicates residual capacity of edge from i to j
     * if there is an edge. If residualGraph.get(i).get(j) is 0, then there is not
     * @param parent path array
     * @param visited boolean array to keep a record of visited nodes
     * @param queue queue with nodes
     */
    private void breadthFirstLoop(List<List<Integer>> residualGraph, List<Integer> parent, List<Boolean> visited, Queue<Integer> queue) {
        while (queue.size() != 0) {
            int removedNode = queue.poll();
            for (int currentNode = 0; currentNode < adjacencyMatrix.size(); ++currentNode) {
                if (!visited.get(currentNode) && residualGraph.get(removedNode).get(currentNode) > 0) {
                    queue.add(currentNode);
                    parent.set(currentNode, removedNode);
                    visited.set(currentNode, true);
                }
            }
        }
    }

    private String getMatrixAsString(List<List<Integer>> adjacencyMatrix) {
        StringBuilder stringBuilder = new StringBuilder();
        adjacencyMatrix.forEach(line -> stringBuilder.append(line).append("\n"));
        return stringBuilder.toString();
    }

    @Override
    public String toString() {
        return getMatrixAsString(adjacencyMatrix);
    }
}
