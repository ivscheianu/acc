package ro.ionutscheianu.tema2.input;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class InputToMatrixConverter {

    List<List<Integer>> input = new ArrayList<>();
    List<List<Integer>> adjacencyMatrix = new ArrayList<>();


    public InputToMatrixConverter() {
        List<String> lines = FileReader.readLinesFromTextFile("src/main/resources/input.txt");
        lines.forEach(line -> input.add(InputParser.parseLine(line)));
    }

    public List<List<Integer>> createAdjacencyListFromInput() {
        allocSpaceForAdjacencyMatrix();
        addSourceNode();
        addStockNode();
        addNodes();
        return adjacencyMatrix;
    }

    public int getCrewNumber(){
        return input.size();
    }

    public int getFlightsNumber(){
        return input.get(0).size();
    }

    private void addNodes() {
        for (int rowIndex = 0; rowIndex < input.size(); ++rowIndex) {
            for (int columnIndex = 0; columnIndex < input.get(rowIndex).size(); ++columnIndex) {
                if (input.get(rowIndex).get(columnIndex) != 0) {
                    addEdge(rowIndex + 1, getFirstFlightNodeNumber() + columnIndex);
                }
            }
        }
    }

    private void addEdge(int sourceNode, int targetNode) {
        adjacencyMatrix.get(sourceNode).set(targetNode, 1);
        adjacencyMatrix.get(targetNode).set(sourceNode, 1);
    }

    private void addSourceNode() {
        Stream.iterate(1, index -> ++index)
                .limit(input.size())
                .forEach(index -> addEdge(getSourceNodeNumber(), index));
    }

    private void addStockNode() {
        Stream.iterate(input.size() + 1, index -> ++index)
                .limit(input.get(0).size())
                .forEach(index -> addEdge(getStockNodeNumber(), index));
    }

    private int getStockNodeNumber() {
        return getSizeOfSquareMatrix() - 1;
    }

    private int getSourceNodeNumber() {
        return 0;
    }

    private void allocSpaceForAdjacencyMatrix() {
        Stream.iterate(0, index -> ++index)
                .limit(getSizeOfSquareMatrix())
                .forEach(index -> adjacencyMatrix.add(new ArrayList<>(Collections.nCopies(getSizeOfSquareMatrix(), 0))));
    }

    private int getFirstFlightNodeNumber() {
        return input.size() + 1;
    }


    private int getSizeOfSquareMatrix() {
        return input.size() + input.get(0).size() + 2;
    }

}
